# HCI

## Usability Heuristics (L2)
* Visibility of system status
  * system should always inform users about its status (loading bar)
* Match between system and real world
  * sys should speak users' language (words, phrases, human readable format)
* User control & freedom
  * users should be always able to exit from unwanted state
* Consistency & standards
  * users should not have to wonder whether different words, situations, or actions mean the same thing
* Error prevention
  * eliminate error-prone conditions or present user with confirmation option before they proceed in the action
  * I.e. calculator - division by 0
* Recognition rather then recall
  * user should not have to remember information from previous dialogues - instructions should be always available and visible
* Flexibility and efficiency of use
  * system should suit inexperienced as well as experienced users
  * allow users to tailor frequent actions
* Aesthetics and minimalist design
  * only relevant data displayed
* Help users recognize, diagnose, and recover from errors
  * error messages should be displayed in plain language (no codes)
* Help and documentation
  * should not be needed but sometimes necessary

## How to sketch (L3)
* Just yacrs
  * It	is	easier	to	draw	lines	with	motions	that:
     * Move	away	from	your	body
  * You	will	achieve	the	best	freedom	of	motion	by using:
     * Elbow as pivot
  * With	respect	to	gripping	your	pencil	you	should:
     * Adjust	your	grip	for	what	you	are	sketching
* Important because:
  * quickly generate & test many ideas
  * way to express and communicate ideas
  * express complex concepts

#### User Persona
* used to describe users and their behaviors into a profile that can inspire design
* name, photo, narrative, scenario


## Controlling (L4)
* **Interaction:** making computers do what we want
* **Intention:** what user wants to do
* **State:** what the system currently believes about what a user wants to do
* requires feedback
* needs to be compatible with user **dynamics** i.e mouse cursor not too fast, keyboard updates immediately
* **Smoothing:** eliminate fluctuations & disturbance
* **Thresholding:** we need to threshold signals to trigger actions (because signals are continuous)

**Signals**
* what we measure about the world
* corrupted by noise and irrelevant observations

![control](control.png)

**Example:** Mouse
* cursor to provide state information
* feedback is presented visually
* displacement of hand is related to the displacement of cursor

#### From reading
* Controlling requires: action, perception, comparison
* **Action**
  * actions needed for control cannot be planed beforehand because world is not
    consistent and predictable
  * e.g. cannot plan how to turn steering wheel before journey
* **Perception**
  * it's what our senses tell us about the world
  * tells us status of whatever it is we are trying to control
* **Comparison and error**
  * **error** does not mean mistake, it means a difference between what is
    perceived and what is intended to be perceived. Example:
    * driver is too much to the right -> turns left
  * 2 pieces of information are needed for **comparison**, actual state and
    targeted state
* **Circular causation**
  * if you want to control behavior, all you need to do is find out which parts
    is the environment are causing the behavior, and manipulate the environment

## States (L5)
#### Finite state machines
* **State:** a state represents a continuing state of the world
* **Action:** an action or transaction is a move between states, it's an event
* system moves from state to another state when action occurs
* actions are assumed to be **instantaneous**
* states **persist** over some period of time

![](states_plus_threshold.png)
##### State types
* **Pointless state**
  * state that cannot change the outcome
* **Railroading**
  * an set of actions with a few backward transitions
* **Reset dependency**
  * action that jumps to the start of interaction

![](bridge_hinge.png)

* should avoid deep hierarchies
* Railroading and reset dependency can be solved by by providing a backward
  option from current state to previous one
* **Rudeness**
  * sequence of states that you can move through, but leads to useless state

### Quantifying Users Research
* **Usability:** the extent to which a product can be used by specified users to
  achieve specified goals with effectiveness, efficiency, and satisfaction in a
  specified context of use.
* 2 types of usability tests
  * **summative** - describing the usability of an applications using metrics
  * **formative** - finding and fixing usability problems
  * The terms formative and summative come from education where they are used
    in a similar way to describe tests of student learning (formative -
    providing immediate feedback to improve learning, versus summative -
    evaluating what was learned).
* it's better to have smaller **representative** sample than large and non
  representative (+ **randomness** matter)
* **Completion rate** or **success rate** = relative frequency
  `(number of success) / (number of all)`
* **Task time:** how long a user spends on an activity
  * **task completion time**
  * **time until failure**
  * **total time on task**
* **Errors** are any unintended action, slip, mistake, or omission a user makes
  while attempting a task.
* **A/B testing:** give users 2 products, compare
* **Open-ended data** examples:
  * Reasons why customers are promoters or detractors for a product.
  * Customer insights from field studies.
  * Product complaints to calls to customer service.
  * Why a task was difficult to complete

## Quantitative Evaluation Techniques (L6)
* **Population** - everyone
* **Sample** - representative group
* **Independent random variable** - what is manipulated
* **Dependent random variable** - what is measured
* often assume that data is normally distributed

### Quantitative Research Crash Course
* Know:
  * discrete vs continuous data
  * categorical vs quantitative
  * population vs sample
  * mean, median, standard deviation, variance
  * p-value, hypothesis testing
* Normal distribution
  * 68% of population within 1 std
  * 95% of population within 2 std
* z-score - number of std from the mean
* Central Limit Theorem
  * as the sample size approaches infinity, the distribution of sample means
    will follow a normal distribution regardless of what the parent population
* margin of error - 5% at the ends of normal distribution

## Quantitative Evaluation Techniques (L7)
#### A standard lab experiment has these key stages
* Provide information to participants about the goals of the experiment and what
  is involved
* Gather informed consent from participants
* Complete a series of tasks to collect data from participants
* Finish the experiment and provide de­‐briefing    

#### Experimental Ethics
* Participants must provide informed consent
* Experimenter must not be in a position of authority over participants
* Participants understand they can withdraw at anytime
* Participants are given the contact information for experimenters

### Basics of Qualitative Research
* **Research Journal**
  * diary in which one records all activities during the
    research. This includes appointments, summaries of discussions, proposal
    writings, problems, dates
  * helps to become self-aware of own biases (not sure how but it's written in
    book :D)
* **Interview**
  * unstructured
        * provide the richest source of data for theory building
        * participants are able to determine what subject to talk about, at what
      pace, in what order, to what depth
        * should allow participants to set the course and take the time they need
  * semi-structured
        * enable researchers to maintain consistency of the interview
        * some topics are chosen before interview but how they are presented in not
          structured
        * researchers may ask additional questions
  * structured
        * conducted using interview guide
* **Observations** may reveal some additional information
  * easy to misinterpret
* participants have right to:
  * opt out anytime without any consequences
  * request deletions of their record
  * remove identifying information
  * anonymity
  * confidentiality
* researchers responsibilities
  * treat users in manner that they would want to be treated <- it was really
    written in the book :D
  * if participant's life is in danger reasearcher has the responsibility to
    notify authorities

## User Experience (L8)
* **Why is qualitative data Important?**
  * User experience is more complex to capture than usability
  * Qualitative data is one of the few ways to understand **why**
  * Rigorous analysis techniques can ensure your results are trustworthy
* **Grounded Theory** is a research method where theory is derived from data
  collection
  * Theoretical concepts are not used to guide data collection, and initial
    research questions can be very open ended
  * Analysis is done through a three part process – Open, Axial, and Selective
    Coding
* **Ethnomethodology:** the study of how peoples’ actions and words serve to
  maintain social order
* **Performance of Self:** use theater and performance studies as a metaphor for
  how people behave in public space

## Analyzing User Experiences (L9)
#### How to
1. **Open Coding**
  * for each quote, concept, important utterance(from interview) add a code
2. **Axial Coding**
  * determine relationships between the open codes
3. **Selective Coding**
  * from the identified groups, give each a code which represents the key
  concept for the group
